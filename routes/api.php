<?php

use App\Http\Controllers\CommentsController;
use App\Http\Controllers\PostController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/news',[PostController::class, 'newsListApi']);

Route::get('/news/{id}',[PostController::class, 'pagePostApi']);

Route::get('news/{id}/comments',[PostController::class, 'commentsPostApi']);

Route::post('news/{id}/comments',[CommentsController::class, 'addPostCommentApi']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
