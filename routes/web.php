<?php

use App\Http\Controllers\CommentsController;
use App\Http\Controllers\PostController;
use App\Models\Post;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//name('home')=используется в header.blade.php чтобы по ссылке всегда возвращаться на главную страницу
Route::get('/', [PostController::class, 'homePage'])->name('home');

//метод регистрирует обработчик на адрес. обработчик это - либо анонимная функция, либо метод контроллера
Route::get('/category/{name}', [PostController::class, 'categoryPage'])->name('category_page');

Route::get('/posts/{id}', [PostController::class, 'pagePost'])->name('page_post');
Route::post('/posts/{id}/comments', [CommentsController::class, 'addPostComment'])->name('create_comment');
