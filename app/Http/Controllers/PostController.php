<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;


class PostController extends Controller
{
    private function getNewsAndCategories()
    {
        //делаем запрос в базу данных для категорий
        $countCategory = Post::selectRaw('category, count(*)')
            ->groupBy('category')
            ->orderByRaw('count(*) desc')
            ->limit(5)->get();
        $categories = $countCategory->pluck('category');

        //делаем запрос в бд для последних 20-ти новостей
        $lastPosts = Post::orderBy('pub_date', 'desc')
            ->limit(20)->get();

        return [
            $categories,
            $lastPosts,
        ];
    }

    public function homePage(Request $request)
    {
        list($categories, $lastPosts) = $this->getNewsAndCategories();
        return view('home-page', ['counts' => $categories, 'lastPosts' => $lastPosts]);
    }

    public function newsListApi(Request $request)
    {
        list($categories, $lastPosts) = $this->getNewsAndCategories();
        return [
            'category' => $categories,
            'lastPosts' => $lastPosts,
        ];
    }

    public function categoryPage(string $name, Request $request)
    {
        //делаем запрос в бд и проверяем, существует ли такая категория.distinct - особая, уникальная категория
        $categoryExists = Post::selectRaw('distinct category')
            ->where('category', $name)->exists();

        if (!$categoryExists) {
            abort(404);
        }
        //делаем запрос в бд, и выводим последние 10 новостей с пагинацией
        $posts = Post::where('category', $name)
            ->orderBy('pub_date', 'desc')
            ->paginate(10);

        return view('category', ['name' => $name, 'posts' => $posts]);
    }


    public function pagePost($idPost, Request $request)
    {
        $post = Post::find($idPost);
        $paginateComment = Comment::where('post_id', $idPost)->paginate(5);

        return view('page-post', ['post' => $post, 'paginateComment' => $paginateComment]);
    }

    public function pagePostApi ($idPost, Request $request) {
        $post = Post::find($idPost);
        return [
            'post' => $post,
        ];
    }

    public function commentsPostApi($idPost, Request $request) {
        $post = Post::find($idPost);
        $paginateComment = Comment::where('post_id', $idPost)->paginate(5);
        return [
            'paginateComment' => $paginateComment,
        ];
    }
}
