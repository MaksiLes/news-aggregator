<?php

namespace App\Http\Controllers;
use App\Models\Comment;

use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function addPostComment(string $postId, Request $request) {
        $comment = new Comment();

        $comment->author_name = $request->input('name');
        $comment->text = $request->input('text_comment');;
        $comment->post_id = $postId;
        $comment->save();

        return back();
    }

    public function addPostCommentApi(string $postId, Request $request) {
        $comment = new Comment();
        $body = $request->json();
        $comment->author_name = $body->get('author_name');
        $comment->text = $body->get('text');
        $comment->post_id = $postId;
        $comment->save();

        return $comment;
    }

}
