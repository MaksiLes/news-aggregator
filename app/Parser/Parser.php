<?php

namespace App\Parser;

use App\Models\Post;

use Illuminate\Support\Collection;

class Parser
{
    public function parse(string $url): Collection
    {
        $xml = new \SimpleXMLElement($url, 0, true);
        $posts = [];
        foreach ($xml->channel->item as $item) {
            $post = new Post();
            $post->author = (string) $item->author;
            $post->title = (string) $item->title;
            $post->link = (string) $item->link;
            $post->description = (string) $item->description;
            $post->pub_date = (string) $item->pubDate;
            $post->category = (string) $item->category;
            $post->source = $url;
            $posts[]=$post;
        }
        return collect($posts);
    }
}
