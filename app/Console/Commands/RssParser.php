<?php

namespace App\Console\Commands;

use App\Models\Post;
use App\Parser\Parser;
use Carbon\Carbon;
use Illuminate\Console\Command;


class RssParser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:rss';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Получает список ссылок на RSS ленты.
    Ссылки по очереди передаются в парсер.
    Парсер возвращает коллекцию новостей, полученных от RSS ленты.';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //из конфигурации получаем список ссылок на RSS ленты
        $rssLinks = config('link.rss_links');
        dump($rssLinks);

        $parser = new Parser();

        foreach ($rssLinks as $rssLink) {
            $count = 0;
            $posts = $parser->parse($rssLink);
            //Ищем в базе данных последнюю новость для этого сайта
            $lastNewsDB = Post::where('source', $rssLink)
                ->orderBy('pub_date', 'desc')->first();

            if ($lastNewsDB === null) {
                foreach ($posts as $item) {
                    $item->save();
                    $count++;
                }
                dump("Мы тут");
            } else {
                //$lastDBPubDate = последняя дата публикации в базе
                $lastDBPubDate = Carbon::create($lastNewsDB->pub_date);
                //$lastRSSPubDate = последняя дата публикации в rss ленте
                $lastRSSPubDate = Carbon::create($posts->first()->pub_date);
                foreach ($posts as $item) {
                    if ($lastDBPubDate < $lastRSSPubDate) {
                        $item->save();
                        $count++;
                    }
                }
            }
            dump('Сайт: ' . $rssLink . '. Cохранено новых новостей: ' . $count);
        }
        return Command::SUCCESS;
    }
}


