<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Post
 *
 * @property int $id
 * @property string $author
 * @property string $title
 * @property string $link
 * @property string $description
 * @property string $pub_date
 * @property string $category
 * @property string $source
 * @method static \Illuminate\Database\Eloquent\Builder|Post newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Post newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Post query()
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post wherePubDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereTitle($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Comment[] $comments
 * @property-read int|null $comments_count
 */
class Post extends Model
{
    use HasFactory;
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    //Массовое присвоение
    protected $fillable = [];
    protected $guarded = [];

    public function getShortDescription() : string
    {
        return mb_substr($this->description, 0, 100) . '...';
    }

    public function getSourceHost() : string {
        return parse_url($this->source, PHP_URL_HOST);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
