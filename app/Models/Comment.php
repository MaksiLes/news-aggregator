<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Comment
 *
 * @property int $id
 * @property int $posts_id
 * @property string $author_name
 * @property string $text
 * @method static \Illuminate\Database\Eloquent\Builder|Comment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Comment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Comment query()
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereAuthorName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment wherePostsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereText($value)
 * @mixin \Eloquent
 * @property int $post_id
 * @property-read \App\Models\Post $post
 * @method static \Illuminate\Database\Eloquent\Builder|Comment wherePostId($value)
 * @property \Illuminate\Support\Carbon $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereCreatedAt($value)
 */
class Comment extends Model
{


    //Массовое присвоение
    protected $fillable = [];
    protected $guarded = [];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
