# News aggregator
## Предустановки
* Создать базу данных (Postgres), заполнить настройки доступа;
* Composer install;
* php artisan migrate;
* php artisan vendor:publish - чтобы правильно отрисовалась пагинация;
* npm run watch - скомпилировать css;
* php artisan serve - запустить сервер.

## Загрузка новостей
Для загрузки новостей в базу через RSS ленты необходимо выполнить команду php artisan parse:rss
