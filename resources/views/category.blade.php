@extends('layout.base')

@section('content')
    <ul class="menu-main">
        <p>{{$name}}</p>
    </ul>
    <main class="main columns">
        <section class="column main-column">
            @foreach($posts as $post)
            <a class="article" href="{{ route('page_post', ['id' => $post->id]) }}">
                <div class="article-body">
                    <h2 class="article-title">{{$post->title}}</h2>
                    <p class="article-content">{{$post->description}}</p>
                    <footer class="article-info">
                        <span>{{$post->author}}</span>
                    </footer>
                </div>
                @endforeach
            </a>
        </section>
    </main>
    <div class="paginate">
        {{ $posts->links('pagination::bootstrap-4') }}
    </div>
@endsection
