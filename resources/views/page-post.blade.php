@extends('layout.base')


@section('content')
    <main class="main columns">
        <a class="pagePost">
            <div class="body-pagePost">
                <h2 class="title-pagePost">{{$post->title}}</h2>
                <p class="content-pagePost">{{$post->description}}</p>
                <footer class="info-pagePost">
                    <span>{{$post->author}}</span>
                </footer>
            </div>
        </a>
    </main>
    <form name="comment"  method="post" action="{{ route('create_comment', ['id' => $post->id]) }}">
        <label>Комментарии:</label>
            <div class="comment">
                <label>Имя:</label> <br>
                    <input type="text" name="name" />
                <p></p>
                    <label>Комментарий:</label>
                <p>
                    <textarea name="text_comment"></textarea>
                </p>
                <p>
                    <input type="submit" class="button" value="Отправить" />
                </p>
            </div>
    </form>
    <div class="commentPosts">
        @foreach($paginateComment as $comment)
        <div class="comment1">
            <p class="p">Имя: {{$comment->author_name}}</p>
            <p class="p">{{$comment->text}}</p>
            <p class="p">{{$comment->created_at}}</p>
        </div>
        @endforeach
    </div>
    <div class="paginate">
        {{ $paginateComment->links('pagination::bootstrap-4') }}
    </div>
@endsection
