@extends('layout.base')

@section('content')
    <ul class="menu-main">
        @foreach($counts as $cat)
            <li><a href="{{ route('category_page', ['name'=>$cat]) }}">{{ $cat }}</a></li>
        @endforeach
    </ul>
    <div class="wrap">
        @foreach($lastPosts as $post)
            <div class="block">
                <div class="zagolovokNews" id="zagolovok">
                    <a href="{{ route('page_post', ['id'=>$post->id]) }}">{{$post->title}}</a>
                </div>
                <div class="description" id="description">{{$post->getShortDescription()}}</div>
                <div class="avtorNews" id="avtor"> Автор: {{$post->author}}</div>
                <div class="source" id="source">{{$post->getSourceHost()}}</div>
                <div class="data" id="data">{{$post->pub_date}}</div>
            </div>
        @endforeach
    </div>
@endsection

