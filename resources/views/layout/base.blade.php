<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<header>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/css/app.css" rel="stylesheet">
</header>
<body>
    @include('layout.header')
    <div class="content">
        @yield('content')
    </div>
    @include('layout.footer')
</body>
</html>
